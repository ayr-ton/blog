require(`dotenv`).config({
  path: `.env`,
})

const shouldAnalyseBundle = process.env.ANALYSE_BUNDLE

module.exports = {
  siteMetadata: {
    siteTitle: `Ayrbag`,
    siteTitleAlt: `Ayrbag`,
    author: `Ayrton Araújo`,
    description: `Também conhecido como bolsa de ar, almofada de ar ou erbegue, este blog é um componente de proteção onde posso errar com segurança.`,
    siteUrl: `https://ayr-ton.net`
  },
  plugins: [
    {
      resolve: `@lekoarts/gatsby-theme-minimal-blog`,
      // See the theme's README for all available options
      options: {
        navigation: [
          {
            title: `Blog`,
            slug: `/blog`,
          },
          {
            title: `Sobre`,
            slug: `/sobre`,
          },
        ],
        externalLinks: [
          {
            name: `Twitter`,
            url: `https://ayr-ton.link/twitter`,
          },
          {
            name: `Instagram`,
            url: `https://ayr-ton.link/instagram`,
          },
          {
            name: `Linkedin`,
            url: `https://ayr-ton.link/linkedin`
          },
          {
            name: `GitHub`,
            url: `https://ayr-ton.link/github`
          }
        ],
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-176546316-1`,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Ayrbag`,
        short_name: `Ayrbag`,
        description: `Também conhecido como bolsa de ar, almofada de ar ou erbegue, este blog é um componente de proteção onde posso errar com segurança.`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#6B46C1`,
        display: `standalone`,
        icons: [
          {
            src: `/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    `gatsby-plugin-offline`,
    shouldAnalyseBundle && {
      resolve: `gatsby-plugin-webpack-bundle-analyser-v2`,
      options: {
        analyzerMode: `static`,
        reportFilename: `_bundle.html`,
        openAnalyzer: false,
      },
    },
  ].filter(Boolean),
}
